{{cookiecutter.project_slug}}
==============================

{{cookiecutter.project_short_description}}

wflow model version
-------------------
{{cookiecutter.wflow_version}}

Project Organization
--------------------

    .
    ├── AUTHORS.md
    ├── CHANGELOG.md
    ├── README.md
    ├── LICENSE
    ├── bin                 <- Your compiled model code can be stored here (not tracked by git)
    ├── config              <- Configuration files, (e.g.: ini file to modelbuilder and wflow model)
    ├── data                
    │   ├── 1-external      <- Data external to the project (e.g. observation data or txt file with paths/url to externally available data)
    │   ├── 2-interim       <- Intermediate data that has been altered from the external data.
    │   ├── 3-modeldata     <- The processed data sets, ready for modeling. With wflow folder structure (NOTE: run folders are not tracked).
    │   |   ├── staticmaps 
    │   |   ├── intbl
    │   |   ├── instate
    │   |   ├── run_default
    │   ├── 4-output        <- Data dump from the model (not tracked by git)
    │   └── 5-visualization <- Post-processed data, ready for visualisation (store only small datasets for tracking by git).
    ├── docs                <- Documentation, e.g., doxygen or scientific papers (not tracked by git)
    ├── notebooks           <- Jupyter notebooks
    ├── reports             <- For a manuscript source, e.g., LaTeX, Markdown, etc., or any project reports
    │   └── figures         <- Figures for the manuscript or reports
    └── src                 <- Source code for this project
        ├── 0-setup         <- Install necessary software, dependencies, pull other git projects, etc.
        ├── 1-prepare       <- Scripts and programs to process data, from 1-external to 2-interim.
        ├── 2-build         <- Scripts to create model specific input from 2-interim to 3-input. 
        ├── 3-model         <- Scripts to run model and convert or compress model results, from 3-input to 4-output.
        ├── 4-analyze       <- Scripts to post-process model results, from 4-output to 5-visualization.
        └── 5-visualize     <- Scripts for visualisation of your results, from 5-visualization to ./report/figures.